
import java.util.*;
//import java.nio.ByteBuffer;
import java.nio.charset.*;
import java.math.BigInteger;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

    ArrayList<Node> letters;
    byte[] original;
    int length;
    ArrayList<Node> leavs;

    // TODO!!! Your instance variables here!

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */

   Huffman (byte[] original) {
       ArrayList<Node> letters = new ArrayList<Node>();
       byte[] sorted = Arrays.copyOf(original, original.length);
       Arrays.sort(sorted);
       for (int i = 0; i < sorted.length; i++ ){
           if (letters.isEmpty()){
               Node node = new Node(String.valueOf(sorted[i]), 1, null, null, null);
               letters.add(node);
           } else if (String.valueOf(sorted[i]).equals( letters.get(letters.size() - 1).letter)){
               letters.get(letters.size() - 1).frequency = letters.get(letters.size() - 1).frequency + 1;
           } else{
               Node node = new Node(String.valueOf(sorted[i]), 1, null, null, null);
               letters.add(node);
           }
       }
       if (letters.size() == 1){
           StringBuffer temp = new StringBuffer();
           temp.append("1");
           letters.get(0).bitCode = temp;
       }
       this.letters = letters;
       this.original = original;
       this.length=0;
       this.leavs = new ArrayList<>();


       // TODO!!! Your constructor here!
   }

    class frequencyCompare implements Comparator<Node>
    {
        public int compare(Node a, Node b)
        {
            return a.frequency - b.frequency;
        }
    }


   class Node {

       String letter;
       int frequency;
       Node left;
       Node right;
       StringBuffer bitCode;

       Node(String letter, int frequency, Node left, Node right, StringBuffer bitCode) {
           this.letter = letter;
           this.frequency = frequency;
           this.left = left;
           this.right = right;
           this.bitCode = bitCode;

       }

       public void assignBitCode(StringBuffer currentBitCode) {
           if (this.left != null) {
               this.left.bitCode = new StringBuffer(currentBitCode.append("0"));
               if (this.right != null) {

                   currentBitCode.deleteCharAt(currentBitCode.length() - 1);
                   this.right.bitCode = new StringBuffer(currentBitCode.append("1"));
               }
               this.left.assignBitCode(new StringBuffer(this.left.bitCode));
           }
           if (this.right != null) {
               this.right.assignBitCode(new StringBuffer(this.right.bitCode));
           }
//           if (this.left != null) {
//               this.left.bitCode = currentBitCode * 2;
//               if (this.right != null) {
//                   this.right.bitCode = this.left.bitCode + 1;
//               }
//               this.left.assignBitCode(this.left.bitCode);
//           }
//           if (this.right != null) {
//               this.right.assignBitCode(this.right.bitCode);
//           }
       }
   }


   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return this.length; // TODO!!!
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {

       letters.sort(new frequencyCompare());
       ArrayList<Node> leavs = new ArrayList<>(letters);
       this.leavs = leavs;
       while (letters.size()>1){ //tree growing
           Node node = new Node(null, letters.get(0).frequency + letters.get(1).frequency,
                   letters.get(0), letters.get(1), null );
           letters.remove(0);
           letters.remove(0);
           letters.add(node);
           letters.sort(new frequencyCompare());
       }

       letters.get(0).assignBitCode(new StringBuffer());
//       for (int i=0; i < leavs.size(); i++){
//           System.out.println(leavs.get(i).bitCode);
//       }
       StringBuffer codedString = new StringBuffer();
       List<Byte> bytes = new ArrayList<Byte>();
       for (byte letter : origData){
         for(int i = leavs.size() - 1; i>=0; i--){
             if (leavs.get(i).letter.equals(String.valueOf(letter))){
                 codedString.append(leavs.get(i).bitCode);
                 bytes.add((byte) Integer.parseInt(leavs.get(i).bitCode.toString(), 2));

             }
         }
       }
       this.length = codedString.length();

        // https://stackoverflow.com/questions/17727310/convert-binary-string-to-byte-array

       byte[] bval = new BigInteger(String.valueOf(codedString), 2).toByteArray();

       byte[] ret = new byte[bytes.size()];
       for (int i = 0; i < bytes.size(); i++)
           ret[i] = bytes.get(i);

       return ret; // TODO!!!
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {

       StringBuffer temp1 = new StringBuffer();
       for (byte b : encodedData){

           temp1.append(Integer.toString(b & 0xFF, 2));
       }

       String code = temp1.toString();
       StringBuffer temp2 = new StringBuffer();
       for (int i = this.length - code.length(); i > 0 ; i--){
           temp2.append("0");
       }
       temp2.append(code);
       code = temp2.toString();
       String[] codeArray = code.split("");

       //System.out.println(this.leavs.get(leavs.size()-1).bitCode);



       StringBuffer temp3 = new StringBuffer();
       StringBuffer temp4 = new StringBuffer();
       List<Byte> bytes = new ArrayList<Byte>();
       for (int j = 0; j < codeArray.length; j ++) {
           temp4.append(codeArray[j]);
           for (int i = this.leavs.size() - 1; i >= 0; i--) {
               if (this.leavs.get(i).bitCode.toString().equals(temp4.toString())){
                   temp3.append(this.leavs.get(i).letter);
                   bytes.add(Byte.valueOf(this.leavs.get(i).letter));
                   temp4.delete(0, temp4.length());
                   break;
               }
           }
       }

       byte[] ret = new byte[bytes.size()];
       for (int i = 0; i < bytes.size(); i++)
           ret[i] = bytes.get(i);
//       String decodedString = temp3.toString();




//       System.out.println(code);
//       System.out.println(temp3.toString());



       return ret; // TODO!!!
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAABBBBBCCCDE";
      //AAAAAAAAAAAAABBBBBBCCCDDEEF
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }
}


